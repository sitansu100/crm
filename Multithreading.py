#              MULITHREADING
#      -----------------------------

# MULTI TASKING
#--------------
#(1) Process Based Multi Tasking
#(2) Thread Based Multi Tasking

'''
(1) Process based Multi tasking -> Executing several tasks simulteniously
      where each task is a seperate independent process is called process based
      multi tasking
(2) Thred based Multi Tasking -> Executing several task simulteniously where
      each task is separate independent of the same program, is called
      Thred based multi tasking
      -> This type of multi tasking is best suitable at programmatic level

'''
# Simple Example
#---------------
'''
from threading import *

def Display():
    print(current_thread().getName())
    for i in range(10):
        print("Chield Thread")
print(current_thread().getName())
t=Thread(target=Display)

t.start()
print(current_thread().getName())
for i in range(10):
    print("Main Thread")
'''
