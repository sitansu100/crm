#        OOPS PART - 3
#       ---------------

# POLYMORPHISM :- -> The word polymorphism means having many forms .
#      -> Polymophism is a very important concept in programming .
#      -> It refers to the use of a single type entity(method,operator
#          or object ) to represent different types in different scenarios.

#Eg:-  + operator acts as string concatination and arithmetic addition .
#Eg:-  * operator acts as multiplication and repetation operation .
'''
class India:
    def Capital(self):
        print("Capital of India is Delhi")
    def Language(self):
        print("National Language is Hindi")
class USA:
    def Capital(self):
        print("Washington, D.C. is the capital of USA.")
    def Language(self):
        print("National Language is English")
class Pak:
    def Capital(self):
        print("Lahore is the capital of USA.")
    def Language(self):
        print("National Language is Urdhu")

obj_ind=India()
obj_us=USA()
obj_pak=Pak()
x=input("Enter Something")
for obj in (obj_ind,obj_us,obj_pak):
    obj.Capital()
    obj.Language()
    print()

'''

# Related To Polymorphism the Following 3 Topics are important

'''
(1) Duck Typing Philosophy
(2) Overloading
    (a)Operator Overloading
    (b)Method Overloading
    (c)Constructor Overloading
(3) Overriding
    (a) Method Overriding
    (b) Constructor Overriding


(1) Duck Typing Philosophy -> In Python we can not specify the type
--------------------------  explicitily . Based on our provided value
   at the runtime the type will be considered autometically . Hence
   python is considered as Dynamically Typed Programming Language .

-> At run time if it walks like a duck , it must be duck . Python follows
this principle . This is called duck Typing philosophy of python .
Note :- The problem of this approach is If the object does not contain
       same method then we will get Attribute Error .
       -> But we can solve this problem by using hasattr() function

'''
'''
class Duck:
    def talk(self):
        print("Quack ... Quack")
class Human:
    def talk(self):
        print("Hello .... hi")
class Dog:
    def Bark(self):
        print("Bow ... Bow")

def f1(obj):
    if hasattr(obj,'talk'):
        obj.talk()
    elif hasattr(obj,'Bark'):
        obj.Bark()
d=Duck()
f1(d)
h=Human()
f1(h)
do=Dog()
f1(do)
'''
'''
(2)Overloading:- We can use same operator or method for different purposes
----------------
#  Types of Overloading
------------------------
(1)Operator Overloading
(2)Method Overloading
(3)Constructor Overloading

(1) Operator Overloading :- We can use same operator for multiple 
 ----------------------- purposes , which is nothing but operator
      overloadig .
      python supports operator overloading .
    -> For every operator Magic method is available . To overload any operator
        we have to override that method .

class Book:
    def __init__(self,pages):
        self.pages=pages
    def __add__(self,other):
        return self.pages+other.pages

b1=Book(100)
b2=Book(200)
print(b1+b2)

(2) Method Overloading :- If 2 methods having same name but different types of
------------------------   arguments then those methods are said to be
            overloaded methods
            Eg :- m1(a)
                  m1(a,b)
            -> But in python Overloading is not possible .
            -> If we are trying to declare multiple methods with same name and
                different number of arguments then Python will always consider
                only last method .


class Test:
    def m1(self):
        print("No-args Method")

    def m1(self,a):
        print("One-arg method")

    def m1(self,a,b):
        print("Two-arg method")

t=Test()
#t.m1() 
#t.m1(10)
t.m1(10,20)
Note - In the above program python will consider only last method .

 (3)Constructor Overloading:- Constructor overloading is not possible in python
 ---------------------------- 
-> If we define multiple constructors then the last constructor will be
     considered .



  OVERRIDING
  -----------

(1) Method Overriding :- What ever members available in parent class are by
----------------------   default available to the child class through
   inheritance. If the child class not satisfy with parent class implementation
   then child class is allowed to redefine that method in child class based
   on its requirement . This concept is called overriding .

   -> Overriding concept applicable for both method and constructor .


class P:
    def property(self):
        print("Gold+Land+Cash+Power")
    def marry(self):
        print("Appalamma")
class C(P):
    def marry(self):
        super().marry()
        print("Katrina Kaif")
c=C()
c.property()
c.marry()

->from overriding method of child class , we can call parent class method also
  by using super() method
'''
