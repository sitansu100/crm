#   GENERATOR FUNCTION
'''
-> Genarator is a function which is responsible to generate a sequence of
    values .
-> We can write generator function just like ordinary functions , but it uses
    yield keyword to return values .
  # ADVANTAGES -> Improves memory Utilization and Performance .
  ------------ -> Generators are best suitable for reading data from large file
  Memory Utilization 
  ------------------
    Normal function -
     l=[x*x for x in range(1000000000000000)]
     print(i[0])  # Output - MemoryError
    Generator function -
     l=(x*x for x in range(10000000000000))
     print(next(l))
     print(next(l))

'''
#  DECORATOR FUNCTINS
#--------------------
'''
-> Decorator is a function which will take a function as argument and extend
   the functionality and returns modified function with extended functionality
-> The main objective of decorator , we can extend the functionality of
    an existing functions without modify that old function .
'''

'''
def decor(func):
    def inner(name):
        if name =='Sitansu':
            print("Hello Sitansu Very Very Good Morning")
        else:
            func(name)
    return inner
    
@decor
def wish(name):
    print("Hello",name,"Good Morning")
wish('Situ')
wish('Sitansu')
'''

# REGULAR EXPRESSIONS
#---------------------
'''
-> If we want to represent a group of string according to a perticular format /
    pattern then we should go for Regular Expression .
-> Regular Expression is a declarative mechanism to represent a group of string
    according to particular format/pattern .
    Ex - We can write regular expression to repesent all Mobile numbers
    Ex - We can write regular expression to represent all Email ids


# IMPORTANT FUNCTIONS
#---------------------
(1) match()
(2) fullmatch()
(3) search()
(4) findall()
(5) finditer()
(6) sub()
(7) subn()
(8) split()
(9) compile()

# match() -> We use match function to check the given pattern present at 
#--------    begining of target string .
          -> If match is available then we will get match object , otherwise
              we will get None .

'''
'''
import re
s= input("Enter pattern to check :")
m=re.match(s,"situsahu")
if m!= None:
    print("Match is available at the beginning of the target string")
else:
    print("Match not available at the beginning of the target string")
'''
# fullmatch() -> We can use fulmmatch() function to match a pattern to all of 
# -----------     target string i.e complete string should be matched according
#                  to given pattern .
# -> If complete string matched then this function returns Match object
#     otherwise it returns None .

'''
import re
s=input("Enter pattern to check :")
m=re.fullmatch(s,'Situ')
if m!= None:
    print("Full string Matched")
else:
    print("Full String Not Matched")
'''

# search() -> We can search() function to search the given pattern in the 
#             target string .
#    -> If the match is available then it returns the match object which
#        represents first occurrence of the match .
#   -> If the match not available then it returns None .

'''
import re
s=input("Enter Pattern to check :")
m=re.search(s,'Hisitu hello how are you situ')
if m!= None:
    print("Match is available")
else:
    print("Match not available")
'''
# findall() -> To find all occurance of the match 
#---------- -> This function returns a list object which contains all occurance
'''
import re
itr=re.findall('[0-9]','ab9a3r5e4t4')
print(itr) # Output -> ['9', '3', '5', '4', '4']
'''
# sub() -> sub means substitution or replacement .
#------ -> re.sub(regs,replacement,targetstring)
'''
import re
s=re.sub('situ','Sitansu','Hi, i am situ')
print(s)
'''

# subn()-> It is exactly same as sub except it can also returns the number  
#-------    of replacement .

#              MULITHREADING
#      -----------------------------

# MULTI TASKING
#--------------
#(1) Process Based Multi Tasking
#(2) Thread Based Multi Tasking

'''
(1) Process based Multi tasking -> Executing several tasks simulteniously
      where each task is a seperate independent process is called process based
      multi tasking
(2) Thred based Multi Tasking -> Executing several task simulteniously where
      each task is separate independent of the same program, is called
      Thred based multi tasking
      -> This type of multi tasking is best suitable at programmatic level

'''
# Simple Example
#---------------
'''
from threading import *

def Display():
    print(current_thread().getName())
    for i in range(10):
        print("Chield Thread")
print(current_thread().getName())
t=Thread(target=Display)

t.start()
print(current_thread().getName())
for i in range(10):
    print("Main Thread")
'''
     # FILE HANDLING
#   -------------------
'''
-> As a part of programming requrement , we have to store our data permanently
    for future purpose . For this requirement we should go for files
-> Files are very  common permanent storage areas to store our data .

# Opening File -> Before performing any operation (like read or write) on the 
#-------------    file , first we have to open the file . For this we should 
                  use Python's inbuild function open()
               -> But at the time of opening , we have to specify the mode ,
                  which represents the purpose of opening file .
#    f=open('xyz.txt','w')
# closing a file -> After completing our operations on the file , It is highly 
#--------------     recomended to close the file . for this we use close()
#                   f.close()

#   Various Modes
(1)-> r -> Open an existing file for read operation . If the file not exist
            then we will get FileNotFoundError .
(2)-> w -> Open an existing file for writing operation . If the file contains 
           some data then it will be overriden . If the specified file not
           exist then this mode will create a new file .
           


#  Various types of Reading Methods

(1) read() -> To read total data from the file .
(2)read(n) -> To read 'n' characters from the file .
(3)readline() -> To read only one line from the file .
(4)readlines() -> To read all lines into a list . 

'''

#          EXCEPTION HANDLING
#       -------------------------
'''
-> In any programming language there are two types of errors are possible .
    (1) Syntax error
    (2) Runtime error

(1) Syntax Error -: The Errors which occurs due to invalid syntax is called 
    ------------     syntax error .
(2) Runtime Error -: While executing the program if something goes wrong
    -------------     because of end user input or programming logic or memory
                      problems etc then we will get runtime error .
                      
# What is Exception ?
# Ans-> An Unwanted and Unexpected event that disturbs normal flow of program 
        is called exception .

        Eg :- ZeroDivisionError
              TypeError
              ValueError
              FileNotFoundError
              EOFError
              SleepingError

 -> The main objective of exception handling is Graceful Termination of the
     program .
 -> Exception handling does not mean repairing exception . We have to define
     alternative way to continue rest of the program normaly .

  -> We can handle exception by using try-except block

  TRY Block :- The code which may raise exception is called risky code , we
               to take risky code inside Try block And the corresponding
               exception handling code we have to take inside except block .
               
  '''
'''
Example :-

try:
    x=int(input("Enter 1st Number :"))
    y=int(input("Enter 2nd Number :"))
    print(x/y)
except ZeroDivisionError:
    print("Can't Devide with Zero")
except ValueError as msg:
    print("Please Provide Int Value Only",msg)
'''
'''
# Finally Block :-
#---------------
-> It is not recomended to maintain clean ip code (Resource Deallocating code
or Resourse Releasing code ) inside Try block because there is no guarentee for
the execution of every statement inside Try Block always .
-> It is not recomended to maintain clean up code inside except block , because
  if there is no exception then except block won't be excuted .

# Else Block :- We can use else block with try-except-finally blocks
#-----------    Else block will be executed if and only if there is no
                exceptions inside Try Block . 

'''
#  OOPS PART - 1

# What is a Class ?
'''
Ans - To create a class we required some plan or model or blue print , which
      is nothing but class .

      Within Python class we can represent data by using variables .
      There are 3 types of variables are allowed .
      (1) Instance Variable (Object level Variables)
      (2) Static Variable (Class level Variables)
      (3) Local Variables (Method level Variables)

      Within python class we can represent operations by using Methods
      (1) Instance Method
      (2) Class Method
      (3) Static Method
'''
# Example For Class
'''
class Student:
    def __init__(self):
        self.name="Situ"
        self.age=29
        self.marks=90
    def talk(self):
        print("Hello I am",self.name)
        print("My Age is :",self.age)
        print("My Mark is :",self.marks)
s=Student()
s.talk()
'''
# What is Object ?
'''
Ans :- Physical existance of a class is nothing but object . We can create any
        number of object for a class
        synatx:- reference variable=classname()
        s=Student()
'''


# What is Reference Variable ?
'''
Ans :- The Variables which can be used to refer a object is called reference
       variables .
       By Using reference variables , we can access properties and method of
       Object .
'''


# Self Variables
#---------------
'''
Ans - Self is the default variable which is always poimting to current object.
      By using self we can access instance variables and instance methods .
      (1) Self Should be the first parameter inside constructor
      (1) self should be first parameter inside Instance Methods
'''


# Constructor Concept
#--------------------
'''
Ans:- (1) Constructor is a special method in python
      (2) The name of constructor should be __init__(self)
      (3) Constructor will be executed automatically at the time of object
           creation.
      (4) The main purpose of constructor is to declare and initialize instance
           variables
      (5) Per object Constructor will execute only Once .
      (6) Contstructor can take atlest one argument (self)
'''
'''
class Student:
    def __init__(self,x,y,z):
        self.name=x
        self.rollno=y
        self.mark=z
    def Display(self):
        print("Student Name:{}\nRoll No.{}\nMark{}".format(self.name,
                                                           self.rollno,
                                                           self.mark))
s1=Student("Situ",101,99)
s1.Display()
print()
s2=Student("Lipu",102,80)
s2.Display()
'''
# Types Of Varianles :
#--------------------
'''
(1) Instance Variables (Object Level Variables)
(2) Static Variables (Class Level Variables)
(3) Local Variables (Mathod Level Variables)

(1) INATANCE VARIABLES :- If the value of variables are varied from object to
        object is called instance variable .
        For Every object a separate copy of instance variable will be created.
    Where we can declare Instance variable ?
    --------------------------------------
    (a) Inside constructer using self variable
    (b) Inside Instance Metode using self Variable
    (c) Outside of the class by using object reference variable
    
    How to Access Instance Variable
    -------------------------------
    -> We can access instance variables within the class by using self
        variable and outside of the class by using object referance .


#class Test:
#    def __init__(self,name,loc):
#        self.name=name
#        self.loc=loc
#    def Display(self):
#        print("Name : ",self.name) # accessing Instance Variable inside 
#        print("Location : ",self.loc)# Instance Method
#t=Test('Situ','Bdk')
#t.name="Sitansu"  
#t.Display()
#print(t.name,t.loc) # accessing Instance Variable outside of the class
#print(t.__dict__)

(2) STATIC VARIABLES :- If the value of variable is not varied from
        object to object then such type of variables we have to declare
        within the class directly but out side of the methods ,
        Such type of variables are called Static variables .
      -> We can access static variables either by class name or by reference .
         But recomended is to use class name .

    Various Places of Declare Static Variables :
    -------------------------------------------
    (a) In General we can declare within the class directly but outside of
          any method
    (b) Insidse constructor by using class name .
    (c) Inside Instance method by using class name .
    (d) Inside class method by using either class name or cls name .
    (e) inside static method by using class name .


# Example various places to declare Static variable .
class Test:
    a=10
    def __init__(self):
        Test.b=20
    def m1(self):
        Test.c=30
        print(self.c)
    @classmethod
    def m2(cls):
        Test.d=40
        cls.e=50
    @staticmethod
    def m3():
        Test.f=50
t=Test()
t.m1()

      How To Access Static Variables
      ------------------------------
      (1) Inside Constructor : By using either self or class name .
      (2) Inside Instance Method : By using either self or class name .
      (3) Inside class Method : By Using either cls variable or class name .
      (4) Inside Static Method : By using class name .
      (5) From outside of class : By using either object reference or class name

class Test:
    a=10
    def __init__(self):
        print(self.a)
        print(Test.a)
    def m1(self):
        print(self.a)
        print(Test.a)
    @classmethod
    def m2(cls):
        print(cls.a)
        print(Test.a)
    @staticmethod
    def m3():
        print(Test.a)
t=Test()
t.m1()
t.m2()
t.m3()

 (3) LOCAL VARIABLES :- Sometimes to meet temporary requirements of programmer,
        we can declare variables inside a method directly , such type of
        variables are called local variable .
        -> Local variables will be careted at the time of method execution
           and destroyed once method completes .

'''
# TYPES OF METHODS
'''
-> Inside python class 3 types of methods are allowed .
(1) Instance method
(2) Class Method
(3) Static Method

(1) INSTANCE METHOD:- Inside method implementation if we are using instance
     variables then such type of methods are called instance method .
     -> Inside instance method declaration , we have to pass self variable
         def m1(self):  <---> Like this
     -> We can access instance variable by using self inside instance method .

class Student:
    def __init__(self,name,marks):
        self.name=name
        self.marks=marks
    def display(self):
        print("Hi",self.name)
        print("Your Mark is :",self.marks)
    def grade(self):
        if self.marks>=60:
            print("You Got 1st grade")
        elif self.marks >=50:
            print('You Got 2nd grade')
        elif self.marks >=35:
            print("You got 3rd grade")
        else:
            print("You are failed")
n=int(input("Enter number of Student :"))
for i in range(n):
    name=input("Enter Student Name :")
    marks=int(input("Enter Marks :"))
    s=Student(name,marks)
    s.display()
    s.grade()
    print()


(2) CLASS METHOD :- Inside method implementation if we are using only static
         variables then such type of methods are called Class Method .
         -> We can declare class method explicitly by using @classmethod
             decorator
         -> For class method we should provide cls variable at the
             time of declaration .
         -> We can call class method by using class name or object referance .

class Animal:
    Legs=4
    @classmethod
    def Description(cls,name):
        print("{} walks with {} legs".format(name,cls.Legs))

Animal.Description("Cat")
Animal.Description("Dog")

(3) Static Method :- These are general utility methods
           -> Inside these method we won't use any instance or class variables
           -> Here we won't provide self or cls arguments at the time of
               declaration .
           -> We can declare static method explicitily by using @staticmethod
               decorator

'''

#     OOPS PART - 2
#---------------------

'''
        INHERITANCE 
        -----------
 -> What ever variables,method and constructor available in paraent class
     by default available to a child class and we are not required to rewrite.
     Hence the main advantages of Inheritance is code reusability .


class P:
    a=10
    def __init__(self):
        self.b=20
    def m1(self):
        print("Parent class Instance Method")
    @classmethod
    def m2(cls):
        print("Parent class classmethod")
    @staticmethod
    def m3():
        print("Parent class staticmethod")
class C(P):
    pass
c=C()
print(c.a)
print(c.b)
c.m1()
c.m2()
c.m3()

#  Types of Inheritance
 -----------------------
 (1) Single Inheritance
 (2) Multi level Inheritance
 (3) Hierarchical Inheritance
 (4) Multiple Inheritance

(1) Single Inheritance -> The Concept of inheriting properties from one class
 ---------------------     to another class is called single inheritance .

(2) Multilevel Inheritance-> The concept of inheriting properties from multiple
 -------------------------  classes to one class with the concept of one after
                             another .

class p:
    def m1(self):
        print("Parent method")
class c(p):
    def m2(self):
        print("Child class method ")
class cc(c):
    def m3(self):
        print("Sub child class method")
c=cc()
c.m3()
c.m2()
c.m1()

 (3)Hirarchical Inheritance-> The concept of inheriting properties from one
 --------------------------  class into multiple child classes is called
                             hirarchical inheritance .


class p:
    def m1(self):
        print("Parant class Method")
class c1(p):
    def m2(self):
        print("Child class 1 method")

class c2(p):
    def m3(self):
        print("Child class 2 method")

c11=c1()
c11.m2()
c11.m1()

# Multiple Inheritance-> The concept of inheriting the properties from multiple
 ---------------------- class into single class at a time, is called multiple
                         inheritance .

class p1:
    def m1(self):
        print("Parent 1 method")
class p2:
    def m2(self):
        print("Parent 2 method")

class c(p1,p2):
    def m3(self):
        print("Child Method")
c=c()
c.m1()
c.m2()


# Super() Method :- Super() is a build in method which is useful to call super
-----------------   class constructers,variables and methods from child class .
#Demo program

class Person:
    def __init__(self,name,age):
        self.name=name
        self.age=age
    def display(self):
        print("Name : ",self.name)
        print("Age : ",self.age)

class Student(Person):
    def __init__(self,name,age,rollno,mark):
        super().__init__(name,age)
        self.rollno=rollno
        self.mark=mark
    def display(self):
        super().display()
        print("Roll No : ",self.rollno)
        print("Mark : ",self.mark)
s=Student("Situ",29,101,99)
s.display()
'''

#        OOPS PART - 3
#       ---------------

# POLYMORPHISM :- -> The word polymorphism means having many forms .
#      -> Polymophism is a very important concept in programming .
#      -> It refers to the use of a single type entity(method,operator
#          or object ) to represent different types in different scenarios.

#Eg:-  + operator acts as string concatination and arithmetic addition .
#Eg:-  * operator acts as multiplication and repetation operation .
'''
class India:
    def Capital(self):
        print("Capital of India is Delhi")
    def Language(self):
        print("National Language is Hindi")
class USA:
    def Capital(self):
        print("Washington, D.C. is the capital of USA.")
    def Language(self):
        print("National Language is English")
class Pak:
    def Capital(self):
        print("Lahore is the capital of USA.")
    def Language(self):
        print("National Language is Urdhu")

obj_ind=India()
obj_us=USA()
obj_pak=Pak()
x=input("Enter Something")
for obj in (obj_ind,obj_us,obj_pak):
    obj.Capital()
    obj.Language()
    print()

'''

# Related To Polymorphism the Following 3 Topics are important

'''
(1) Duck Typing Philosophy
(2) Overloading
    (a)Operator Overloading
    (b)Method Overloading
    (c)Constructor Overloading
(3) Overriding
    (a) Method Overriding
    (b) Constructor Overriding


(1) Duck Typing Philosophy -> In Python we can not specify the type
--------------------------  explicitily . Based on our provided value
   at the runtime the type will be considered autometically . Hence
   python is considered as Dynamically Typed Programming Language .

-> At run time if it walks like a duck , it must be duck . Python follows
this principle . This is called duck Typing philosophy of python .
Note :- The problem of this approach is If the object does not contain
       same method then we will get Attribute Error .
       -> But we can solve this problem by using hasattr() function

'''
'''
class Duck:
    def talk(self):
        print("Quack ... Quack")
class Human:
    def talk(self):
        print("Hello .... hi")
class Dog:
    def Bark(self):
        print("Bow ... Bow")

def f1(obj):
    if hasattr(obj,'talk'):
        obj.talk()
    elif hasattr(obj,'Bark'):
        obj.Bark()
d=Duck()
f1(d)
h=Human()
f1(h)
do=Dog()
f1(do)
'''
'''
(2)Overloading:- We can use same operator or method for different purposes
----------------
#  Types of Overloading
------------------------
(1)Operator Overloading
(2)Method Overloading
(3)Constructor Overloading

(1) Operator Overloading :- We can use same operator for multiple 
 ----------------------- purposes , which is nothing but operator
      overloadig .
      python supports operator overloading .
    -> For every operator Magic method is available . To overload any operator
        we have to override that method .

class Book:
    def __init__(self,pages):
        self.pages=pages
    def __add__(self,other):
        return self.pages+other.pages

b1=Book(100)
b2=Book(200)
print(b1+b2)

(2) Method Overloading :- If 2 methods having same name but different types of
------------------------   arguments then those methods are said to be
            overloaded methods
            Eg :- m1(a)
                  m1(a,b)
            -> But in python Overloading is not possible .
            -> If we are trying to declare multiple methods with same name and
                different number of arguments then Python will always consider
                only last method .


class Test:
    def m1(self):
        print("No-args Method")

    def m1(self,a):
        print("One-arg method")

    def m1(self,a,b):
        print("Two-arg method")

t=Test()
#t.m1() 
#t.m1(10)
t.m1(10,20)
Note - In the above program python will consider only last method .

 (3)Constructor Overloading:- Constructor overloading is not possible in python
 ---------------------------- 
-> If we define multiple constructors then the last constructor will be
     considered .



  OVERRIDING
  -----------

(1) Method Overriding :- What ever members available in parent class are by
----------------------   default available to the child class through
   inheritance. If the child class not satisfy with parent class implementation
   then child class is allowed to redefine that method in child class based
   on its requirement . This concept is called overriding .

   -> Overriding concept applicable for both method and constructor .


class P:
    def property(self):
        print("Gold+Land+Cash+Power")
    def marry(self):
        print("Appalamma")
class C(P):
    def marry(self):
        super().marry()
        print("Katrina Kaif")
c=C()
c.property()
c.marry()

->from overriding method of child class , we can call parent class method also
  by using super() method
'''




















































                















      

































        









        















































        











 


























        

















    

 
    
















 





































































        


























        
    



































        












































  
