#   GENERATOR FUNCTION
'''
-> Genarator is a function which is responsible to generate a sequence of
    values .
-> We can write generator function just like ordinary functions , but it uses
    yield keyword to return values .
  # ADVANTAGES -> Improves memory Utilization and Performance .
  ------------ -> Generators are best suitable for reading data from large file
  Memory Utilization 
  ------------------
    Normal function -
     l=[x*x for x in range(1000000000000000)]
     print(i[0])  # Output - MemoryError
    Generator function -
     l=(x*x for x in range(10000000000000))
     print(next(l))
     print(next(l))

'''
