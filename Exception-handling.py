
#          EXCEPTION HANDLING
#       -------------------------
'''
-> In any programming language there are two types of errors are possible .
    (1) Syntax error
    (2) Runtime error

(1) Syntax Error -: The Errors which occurs due to invalid syntax is called 
    ------------     syntax error .
(2) Runtime Error -: While executing the program if something goes wrong
    -------------     because of end user input or programming logic or memory
                      problems etc then we will get runtime error .
                      
# What is Exception ?
# Ans-> An Unwanted and Unexpected event that disturbs normal flow of program 
        is called exception .

        Eg :- ZeroDivisionError
              TypeError
              ValueError
              FileNotFoundError
              EOFError
              SleepingError

 -> The main objective of exception handling is Graceful Termination of the
     program .
 -> Exception handling does not mean repairing exception . We have to define
     alternative way to continue rest of the program normaly .

  -> We can handle exception by using try-except block

  TRY Block :- The code which may raise exception is called risky code , we
               to take risky code inside Try block And the corresponding
               exception handling code we have to take inside except block .
               
  '''
'''
Example :-

try:
    x=int(input("Enter 1st Number :"))
    y=int(input("Enter 2nd Number :"))
    print(x/y)
except ZeroDivisionError:
    print("Can't Devide with Zero")
except ValueError as msg:
    print("Please Provide Int Value Only",msg)
'''
'''
# Finally Block :-
#---------------
-> It is not recomended to maintain clean ip code (Resource Deallocating code
or Resourse Releasing code ) inside Try block because there is no guarentee for
the execution of every statement inside Try Block always .
-> It is not recomended to maintain clean up code inside except block , because
  if there is no exception then except block won't be excuted .

# Else Block :- We can use else block with try-except-finally blocks
#-----------    Else block will be executed if and only if there is no
                exceptions inside Try Block . 

