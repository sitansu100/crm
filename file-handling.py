     # FILE HANDLING
#   -------------------
'''
-> As a part of programming requrement , we have to store our data permanently
    for future purpose . For this requirement we should go for files
-> Files are very  common permanent storage areas to store our data .

# Opening File -> Before performing any operation (like read or write) on the 
#-------------    file , first we have to open the file . For this we should 
                  use Python's inbuild function open()
               -> But at the time of opening , we have to specify the mode ,
                  which represents the purpose of opening file .
#    f=open('xyz.txt','w')
# closing a file -> After completing our operations on the file , It is highly 
#--------------     recomended to close the file . for this we use close()
#                   f.close()

#   Various Modes
(1)-> r -> Open an existing file for read operation . If the file not exist
            then we will get FileNotFoundError .
(2)-> w -> Open an existing file for writing operation . If the file contains 
           some data then it will be overriden . If the specified file not
           exist then this mode will create a new file .
           


#  Various types of Reading Methods

(1) read() -> To read total data from the file .
(2)read(n) -> To read 'n' characters from the file .
(3)readline() -> To read only one line from the file .
(4)readlines() -> To read all lines into a list . 

'''
