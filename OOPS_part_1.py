#  OOPS PART - 1

# What is a Class ?
'''
Ans - To create a class we required some plan or model or blue print , which
      is nothing but class .

      Within Python class we can represent data by using variables .
      There are 3 types of variables are allowed .
      (1) Instance Variable (Object level Variables)
      (2) Static Variable (Class level Variables)
      (3) Local Variables (Method level Variables)

      Within python class we can represent operations by using Methods
      (1) Instance Method
      (2) Class Method
      (3) Static Method
'''
# Example For Class
'''
class Student:
    def __init__(self):
        self.name="Situ"
        self.age=29
        self.marks=90
    def talk(self):
        print("Hello I am",self.name)
        print("My Age is :",self.age)
        print("My Mark is :",self.marks)
s=Student()
s.talk()
'''
# What is Object ?
'''
Ans :- Physical existance of a class is nothing but object . We can create any
        number of object for a class
        synatx:- reference variable=classname()
        s=Student()
'''


# What is Reference Variable ?
'''
Ans :- The Variables which can be used to refer a object is called reference
       variables .
       By Using reference variables , we can access properties and method of
       Object .
'''


# Self Variables
#---------------
'''
Ans - Self is the default variable which is always poimting to current object.
      By using self we can access instance variables and instance methods .
      (1) Self Should be the first parameter inside constructor
      (1) self should be first parameter inside Instance Methods
'''


# Constructor Concept
#--------------------
'''
Ans:- (1) Constructor is a special method in python
      (2) The name of constructor should be __init__(self)
      (3) Constructor will be executed automatically at the time of object
           creation.
      (4) The main purpose of constructor is to declare and initialize instance
           variables
      (5) Per object Constructor will execute only Once .
      (6) Contstructor can take atlest one argument (self)
'''
'''
class Student:
    def __init__(self,x,y,z):
        self.name=x
        self.rollno=y
        self.mark=z
    def Display(self):
        print("Student Name:{}\nRoll No.{}\nMark{}".format(self.name,
                                                           self.rollno,
                                                           self.mark))
s1=Student("Situ",101,99)
s1.Display()
print()
s2=Student("Lipu",102,80)
s2.Display()
'''
# Types Of Varianles :
#--------------------
'''
(1) Instance Variables (Object Level Variables)
(2) Static Variables (Class Level Variables)
(3) Local Variables (Mathod Level Variables)

(1) INATANCE VARIABLES :- If the value of variables are varied from object to
        object is called instance variable .
        For Every object a separate copy of instance variable will be created.
    Where we can declare Instance variable ?
    --------------------------------------
    (a) Inside constructer using self variable
    (b) Inside Instance Metode using self Variable
    (c) Outside of the class by using object reference variable
    
    How to Access Instance Variable
    -------------------------------
    -> We can access instance variables within the class by using self
        variable and outside of the class by using object referance .


#class Test:
#    def __init__(self,name,loc):
#        self.name=name
#        self.loc=loc
#    def Display(self):
#        print("Name : ",self.name) # accessing Instance Variable inside 
#        print("Location : ",self.loc)# Instance Method
#t=Test('Situ','Bdk')
#t.name="Sitansu"  
#t.Display()
#print(t.name,t.loc) # accessing Instance Variable outside of the class
#print(t.__dict__)

(2) STATIC VARIABLES :- If the value of variable is not varied from
        object to object then such type of variables we have to declare
        within the class directly but out side of the methods ,
        Such type of variables are called Static variables .
      -> We can access static variables either by class name or by reference .
         But recomended is to use class name .

    Various Places of Declare Static Variables :
    -------------------------------------------
    (a) In General we can declare within the class directly but outside of
          any method
    (b) Insidse constructor by using class name .
    (c) Inside Instance method by using class name .
    (d) Inside class method by using either class name or cls name .
    (e) inside static method by using class name .


# Example various places to declare Static variable .
class Test:
    a=10
    def __init__(self):
        Test.b=20
    def m1(self):
        Test.c=30
        print(self.c)
    @classmethod
    def m2(cls):
        Test.d=40
        cls.e=50
    @staticmethod
    def m3():
        Test.f=50
t=Test()
t.m1()

      How To Access Static Variables
      ------------------------------
      (1) Inside Constructor : By using either self or class name .
      (2) Inside Instance Method : By using either self or class name .
      (3) Inside class Method : By Using either cls variable or class name .
      (4) Inside Static Method : By using class name .
      (5) From outside of class : By using either object reference or class name

class Test:
    a=10
    def __init__(self):
        print(self.a)
        print(Test.a)
    def m1(self):
        print(self.a)
        print(Test.a)
    @classmethod
    def m2(cls):
        print(cls.a)
        print(Test.a)
    @staticmethod
    def m3():
        print(Test.a)
t=Test()
t.m1()
t.m2()
t.m3()

 (3) LOCAL VARIABLES :- Sometimes to meet temporary requirements of programmer,
        we can declare variables inside a method directly , such type of
        variables are called local variable .
        -> Local variables will be careted at the time of method execution
           and destroyed once method completes .

'''
# TYPES OF METHODS
'''
-> Inside python class 3 types of methods are allowed .
(1) Instance method
(2) Class Method
(3) Static Method

(1) INSTANCE METHOD:- Inside method implementation if we are using instance
     variables then such type of methods are called instance method .
     -> Inside instance method declaration , we have to pass self variable
         def m1(self):  <---> Like this
     -> We can access instance variable by using self inside instance method .

class Student:
    def __init__(self,name,marks):
        self.name=name
        self.marks=marks
    def display(self):
        print("Hi",self.name)
        print("Your Mark is :",self.marks)
    def grade(self):
        if self.marks>=60:
            print("You Got 1st grade")
        elif self.marks >=50:
            print('You Got 2nd grade')
        elif self.marks >=35:
            print("You got 3rd grade")
        else:
            print("You are failed")
n=int(input("Enter number of Student :"))
for i in range(n):
    name=input("Enter Student Name :")
    marks=int(input("Enter Marks :"))
    s=Student(name,marks)
    s.display()
    s.grade()
    print()


(2) CLASS METHOD :- Inside method implementation if we are using only static
         variables then such type of methods are called Class Method .
         -> We can declare class method explicitly by using @classmethod
             decorator
         -> For class method we should provide cls variable at the
             time of declaration .
         -> We can call class method by using class name or object referance .

class Animal:
    Legs=4
    @classmethod
    def Description(cls,name):
        print("{} walks with {} legs".format(name,cls.Legs))

Animal.Description("Cat")
Animal.Description("Dog")

(3) Static Method :- These are general utility methods
           -> Inside these method we won't use any instance or class variables
           -> Here we won't provide self or cls arguments at the time of
               declaration .
           -> We can declare static method explicitily by using @staticmethod
               decorator.

'''



               
