
#     OOPS PART - 2
#---------------------

'''
        INHERITANCE 
        -----------
 -> What ever variables,method and constructor available in paraent class
     by default available to a child class and we are not required to rewrite.
     Hence the main advantages of Inheritance is code reusability .


class P:
    a=10
    def __init__(self):
        self.b=20
    def m1(self):
        print("Parent class Instance Method")
    @classmethod
    def m2(cls):
        print("Parent class classmethod")
    @staticmethod
    def m3():
        print("Parent class staticmethod")
class C(P):
    pass
c=C()
print(c.a)
print(c.b)
c.m1()
c.m2()
c.m3()

#  Types of Inheritance
 -----------------------
 (1) Single Inheritance
 (2) Multi level Inheritance
 (3) Hierarchical Inheritance
 (4) Multiple Inheritance

(1) Single Inheritance -> The Concept of inheriting properties from one class
 ---------------------     to another class is called single inheritance .

(2) Multilevel Inheritance-> The concept of inheriting properties from multiple
 -------------------------  classes to one class with the concept of one after
                             another .

class p:
    def m1(self):
        print("Parent method")
class c(p):
    def m2(self):
        print("Child class method ")
class cc(c):
    def m3(self):
        print("Sub child class method")
c=cc()
c.m3()
c.m2()
c.m1()

 (3)Hirarchical Inheritance-> The concept of inheriting properties from one
 --------------------------  class into multiple child classes is called
                             hirarchical inheritance .


class p:
    def m1(self):
        print("Parant class Method")
class c1(p):
    def m2(self):
        print("Child class 1 method")

class c2(p):
    def m3(self):
        print("Child class 2 method")

c11=c1()
c11.m2()
c11.m1()

# Multiple Inheritance-> The concept of inheriting the properties from multiple
 ---------------------- class into single class at a time, is called multiple
                         inheritance .

class p1:
    def m1(self):
        print("Parent 1 method")
class p2:
    def m2(self):
        print("Parent 2 method")

class c(p1,p2):
    def m3(self):
        print("Child Method")
c=c()
c.m1()
c.m2()


# Super() Method :- Super() is a build in method which is useful to call super
-----------------   class constructers,variables and methods from child class .
#Demo program

class Person:
    def __init__(self,name,age):
        self.name=name
        self.age=age
    def display(self):
        print("Name : ",self.name)
        print("Age : ",self.age)

class Student(Person):
    def __init__(self,name,age,rollno,mark):
        super().__init__(name,age)
        self.rollno=rollno
        self.mark=mark
    def display(self):
        super().display()
        print("Roll No : ",self.rollno)
        print("Mark : ",self.mark)
s=Student("Situ",29,101,99)
s.display()

'''

