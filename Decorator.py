#  DECORATOR FUNCTINS
#--------------------
'''
-> Decorator is a function which will take a function as argument and extend
   the functionality and returns modified function with extended functionality
-> The main objective of decorator , we can extend the functionality of
    an existing functions without modify that old function .
'''

'''
def decor(func):
    def inner(name):
        if name =='Sitansu':
            print("Hello Sitansu Very Very Good Morning")
        else:
            func(name)
    return inner
    
@decor
def wish(name):
    print("Hello",name,"Good Morning")
wish('Situ')
wish('Sitansu')
'''
