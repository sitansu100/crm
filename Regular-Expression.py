# REGULAR EXPRESSIONS
#---------------------
'''
-> If we want to represent a group of string according to a perticular format /
    pattern then we should go for Regular Expression .
-> Regular Expression is a declarative mechanism to represent a group of string
    according to particular format/pattern .
    Ex - We can write regular expression to repesent all Mobile numbers
    Ex - We can write regular expression to represent all Email ids


# IMPORTANT FUNCTIONS
#---------------------
(1) match()
(2) fullmatch()
(3) search()
(4) findall()
(5) finditer()
(6) sub()
(7) subn()
(8) split()
(9) compile()

# match() -> We use match function to check the given pattern present at 
#--------    begining of target string .
          -> If match is available then we will get match object , otherwise
              we will get None .

'''
'''
import re
s= input("Enter pattern to check :")
m=re.match(s,"situsahu")
if m!= None:
    print("Match is available at the beginning of the target string")
else:
    print("Match not available at the beginning of the target string")
'''
# fullmatch() -> We can use fulmmatch() function to match a pattern to all of 
# -----------     target string i.e complete string should be matched according
#                  to given pattern .
# -> If complete string matched then this function returns Match object
#     otherwise it returns None .

'''
import re
s=input("Enter pattern to check :")
m=re.fullmatch(s,'Situ')
if m!= None:
    print("Full string Matched")
else:
    print("Full String Not Matched")
'''

# search() -> We can search() function to search the given pattern in the 
#             target string .
#    -> If the match is available then it returns the match object which
#        represents first occurrence of the match .
#   -> If the match not available then it returns None .

'''
import re
s=input("Enter Pattern to check :")
m=re.search(s,'Hisitu hello how are you situ')
if m!= None:
    print("Match is available")
else:
    print("Match not available")
'''
# findall() -> To find all occurance of the match 
#---------- -> This function returns a list object which contains all occurance
'''
import re
itr=re.findall('[0-9]','ab9a3r5e4t4')
print(itr) # Output -> ['9', '3', '5', '4', '4']
'''
# sub() -> sub means substitution or replacement .
#------ -> re.sub(regs,replacement,targetstring)
'''
import re
s=re.sub('situ','Sitansu','Hi, i am situ')
print(s)
'''

# subn()-> It is exactly same as sub except it can also returns the number  
#-------    of replacement .
